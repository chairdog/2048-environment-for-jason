won :- ~loss.
loss :- ~won.

none_zero_tile(X,Y,V) :- tile(X,Y,V) & V\==0.

valid_right :- none_zero_tile(X,Y,V) & (tile(X,Y+1,0) | tile(X,Y+1,V)).
valid_left :- none_zero_tile(X,Y,V) & (tile(X,Y-1,0) | tile(X,Y-1,V)).                                                                  
valid_up :- none_zero_tile(X,Y,V) & (tile(X-1,Y,0) | tile(X-1,Y,V)).
valid_down :- none_zero_tile(X,Y,V) & (tile(X+1,Y,0) | tile(X+1,Y,V)).