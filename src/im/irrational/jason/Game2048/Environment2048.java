package im.irrational.jason.Game2048;

/**
 * Created by yg452 on 3/11/16.
 */

import jason.asSyntax.Literal;
import jason.asSyntax.Structure;
import jason.asSyntax.Term;
import jason.environment.Environment;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Logger;

public class Environment2048 extends Environment {
    private Logger logger = Logger.getLogger(Environment2048.class.getName());
    Game game;
    GUI gui = null;

    Term    up      = Literal.parseLiteral("up");
    Term    down    = Literal.parseLiteral("down");
    Term    right   = Literal.parseLiteral("right");
    Term    left    = Literal.parseLiteral("left");
    @Override
    public void init(String[] args) {
        final Random random = new Random();
        game = new Game(random);
        //game.savePath = new File("/home/yg452/Documents");
        game.reset();
        if (args.length == 1 && args[0].equals("gui")) {
            gui = new GUI(game, this);
            final ActionListener taskPerformer = e -> gui.update();
            new Timer(100, taskPerformer).start();
        }
        updatePercepts();
    }

    @Override
    public void stop() {
        // anything else to be done by the environment when the system is stopped
        // TODO: 3/11/16
    }

    @Override
    public boolean executeAction(String ag, Structure act) {
        boolean result = true;
//        switch (act.getFunctor()) {
//            case "up":
//                game.playgame(0);
//                break;
//            case "right":
//                game.playgame(1);
//                break;
//            case "down":
//                game.playgame(2);
//                break;
//            case "left":
//                game.playgame(3);
//                break;
//            default:
//                result = false; // fail due to unknown action
//        }
        System.out.println("["+ag+"] doing: "+act);
        if (act.equals(up)){
            logger.finest("moving up");
            game.playGame(0);
        }
        else if (act.equals(right)){
            logger.finest("moving right");
            game.playGame(1);
        }
        else if (act.equals(down)){
            logger.finest("moving down");
            game.playGame(2);
        }
        else if (act.equals(left)){
            logger.finest("moving left");
            game.playGame(3);
        }
        else{
            logger.info("executing: " + act + ", but not implemented!");
            result = false;
        }
        if (result) {// update percepts
            updatePercepts();
        }
        return result;
    }

    private void updatePercepts() {
        // clear the percepts of the agent
        clearPercepts(); //remove previous percepts
        // add grid
        addPercept(Literal.parseLiteral(String.format("rows(%d)", game.getGrid().length)));
        addPercept(Literal.parseLiteral(String.format("columns(%d)", game.getGrid()[0].length)));
        addPercept(Literal.parseLiteral(String.format("grid(%s)", Arrays.deepToString(game.getGrid()))));
        int[][] grid = game.getGrid();
        for(int i=0; i<grid.length; i++ ){
            for (int j=0; j<grid[i].length; j++){
                //if(grid[i][j] != Game.EMPTY) {
                    addPercept(Literal.parseLiteral(String.format("tile(%d,%d,%d)", i, j, grid[i][j])));
                //}
            }
        }
        // add won/loss
        if (game.getHasWon() != null) {
            if (game.getHasWon()) {
                addPercept(Literal.parseLiteral("won"));
            } else {
                addPercept(Literal.parseLiteral("loss"));
            }
        }
        // add score
        addPercept(Literal.parseLiteral(String.format("score(%d)", game.getScore())));
    }

    public void reset() {
        game.reset();
        updatePercepts();
        addPercept(Literal.parseLiteral("restart"));
    }
}
