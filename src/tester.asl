/* Initial beliefs and rules */
{ include("rules.asl") }

/* Initial goals */
!play.

/* Plans */
+!play : won
	<-	.succeed_goal(play).

+!play : loss
	<-	.fail_goal(play).
	
+!play : true
	<-	!update_game_state;
		!make_move;
		!play.


+!make_move : valid_left
	<-	left.
	
+!make_move : valid_up
	<-	up.
	
+!make_move : valid_down
	<-	down.
	
+!make_move : valid_right
	<-	right.
		
		
+!update_game_state : true
	<-	true.
	
+restart : true
	<-	!play.
	
	